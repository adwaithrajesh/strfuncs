CC = gcc


.PHONY: all
all: libstrfuncs.so test

libstrfuncs.so: ./src/strfuncs.c
	$(CC) -shared -fPIC -Wall $< -o $@


.PHONY: test
test: cffi_builder pytest

.PHONY: cffi_builder
cffi_builder: builder.py
	python3 builder.py

.PHONY: pytest
pytest:
	pytest -vv

.PHONY: clean
clean:
	rm -f libstrfuncs.so a.out bin/* ./src/strfuncs.o ./str_lib/*
