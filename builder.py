# this file is just to build the cPython c wrapper for the library so that
# it can be tested using pytest


from cffi import FFI


CDEF = '''\
int in_string(char *, char, int);
size_t char_count(char *, char);
int is_lower(char *);
int is_upper(char *);
int is_alpha(char *);
int is_digit(char *);
int startswith(char *, char *);
'''

ffibuilder = FFI()
ffibuilder.cdef(CDEF)
ffibuilder.set_source(
    'str_lib._lib_strfuncs',
    '''\
    #include <stdlib.h>
    #include <string.h>
    #include <strfuncs.h>
    ''',
    sources=['./src/strfuncs.c'],
    include_dirs=['./src']
)

if __name__ == '__main__':
    ffibuilder.compile(verbose=True, debug=False)
