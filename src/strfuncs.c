#include <stdlib.h>
#include <string.h>

int in_string(char *arr, char c, int arr_len) {
    int found = 0;
    for (int i = 0; i < arr_len; i++) {
        if (*(arr + i) == c) {
            found = 1;
            break;
        }
    }

    return found;
}

size_t char_count(char *str, char c) {
    size_t count = 0;

    for (size_t i = 0; i < strlen(str); i++) {
        if (*(str + i) == c) {
            count++;
        }
    }
    return count;
}

int is_lower(char *str) {
    for (size_t i = 0; i < strlen(str); i++) {
        if (((65 <= *(str + i)) && (90 >= *(str + i)))) {
            return 0;
        }
    }

    return 1;
}

int is_upper(char *str) {
    for (size_t i = 0; i < strlen(str); i++) {
        if (((97 <= *(str + i)) && (122 >= *(str + i)))) {
            return 0;
        }
    }

    return 1;
}

int is_alpha(char *str) {
    for (size_t i = 0; i < strlen(str); i++) {
        if (!(((65 <= *(str + i)) && (90 >= *(str + i))) ||
              ((97 <= *(str + i)) && (122 >= *(str + i))))) {
            return 0;
        }
    }

    return 1;
}

int is_digit(char *str) {
    for (size_t i = 0; i < strlen(str); i++) {
        if (!((48 <= *(str + i)) && (57 >= *(str + i)))) return 0;
    }

    return 1;
}

int startswith(char *str, char *sub) {
    size_t str_len = strlen(str);
    size_t sub_len = strlen(sub);

    if (sub_len > str_len) return 0;

    for (size_t i = 0; i < sub_len; i++) {
        if (!(*(str + i) == *(sub + i))) return 0;
    }

    return 1;
}