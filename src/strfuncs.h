#ifndef STRFUNCS_H
#define STRFUNCS_H

#include <stdlib.h>

int in_string(char *, char, int);
size_t char_count(char *, char);
int is_lower(char *);
int is_upper(char *);
int is_alpha(char *);
int is_digit(char *);
int startswith(char *, char *);

#endif
