import pytest

import str_lib._lib_strfuncs

lib = str_lib._lib_strfuncs.lib
ffi = str_lib._lib_strfuncs.ffi


def se(string: str) -> bytes:
    # string_encode
    return string.encode('ascii')


@pytest.mark.parametrize(
    'string,c,count',
    (
        ('Adwaith', 'a', 1),
        ('adwaith', 'a', 2),
        ('ab'*30, 'b', 30),
        ('23'*300, '3', 300),
    )
)
def test_char_count(string, c, count):
    assert lib.char_count(se(string), se(c)) == count


@pytest.mark.parametrize(
    'string,c,val',
    (
        ('Abcd', 'A', 1),
        ('acd', 'b', 0)
    )
)
def test_in_string(string, c, val):
    assert lib.in_string(se(string), se(c), len(string)) == val


@pytest.mark.parametrize(
    'string,val',
    (
        ('abcde', 1),
        ('a3423bf', 1),
        ('AD', 0)
    )
)
def test_is_lower(string, val):
    assert lib.is_lower(se(string)) == val


@pytest.mark.parametrize(
    'string,val',
    (
        ('ABCD', 1),
        ('ABC323DF', 1),
        ('av', 0)
    )
)
def test_is_upper(string, val):
    assert lib.is_upper(se(string)) == val


@pytest.mark.parametrize(
    'string,val',
    (
        ('abdergg', 1),
        ('ADWAD', 1),
        ('asddfeSFSD', 1),
        ('234234', 0),
        ('fef44', 0)
    )
)
def test_is_alpha(string, val):
    assert lib.is_alpha(se(string)) == val


@pytest.mark.parametrize(
    'string,val',
    (
        ('345345', 1),
        ('3', 1),
        ('234234w', 0),
        ('efwef', 0)
    )
)
def test_is_digit(string, val):
    assert lib.is_digit(se(string)) == val


@pytest.mark.parametrize(
    'string,sub,val',
    (
        ('hello', 'he', 1),
        ('blah', 'blah', 1),
        ('zero', 'ab', 0),
        ('zero', 'zeros', 0)
    )
)
def test_startswith(string, sub, val):
    assert lib.startswith(se(string), se(sub)) == val
